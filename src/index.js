const express = require('express');
const app = express();

app.get('/hc', (req, res) => {
    res.send('ok');
});

app.listen(3000, () => {
    console.log('App listening on 0.0.0.0:3000...')
});
