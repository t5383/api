FROM node:16

WORKDIR /app
COPY package*.json ./
RUN npm install && npm install -g nodemon

COPY ./src ./src
EXPOSE 3000
CMD [ "node", "src/index.js" ]
